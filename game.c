#include "game.h"
#include "screen.h"


void gameInit(){
	keypad_init();
	posx1 = 70;
	posy1 = 26;
	height1 = 4;
	width1 = 3;
	accy1 = 1;
	vely1 = 0;
	accx1 = 0;
	velx1 = 0;
	state1 = 0;
}

void gamePhysics(){
	if(posy1 >= 25){
		vely1 = 0;
		posy1 = 25;
		state1 = 0;
	}
	else if(posy1 <= 10 && accy1 == -1){
		accy1 = 1;
		posy1 = 10;
		vely1 = 0;
		state1 = 1;
	}
	else{
		vely1 = vely1 + accy1; 
		posy1 = posy1 + vely1;
	}
}

void render(){
	clear_screen();
	if(state1 == 0 || state1 == 1){
		for(int i = posy1, y = 0; i < posy1 + height1; i++, y++){
			for(int j = posx1, x = 0; j < posx1 + width1; j++, x++){
				set_cursor(i, j);
				put_char(p1[y][x]);
			}
		}
	}
	else if(state1 == 2){
		for(int i = posy1, y = 0; i < posy1 + height1; i++, y++){
			for(int j = posx1, x = 0; j < posx1 + width1; j++, x++){
				set_cursor(i, j);
				put_char(p1a[y][x]);
			}
		}
	}
}

void gameLoop(){
	gameInit();
	while(true){
    	uint8_t key = keypad_getkey();
    	render();
    	gamePhysics();
    	switch(key){
    		case 1://L
    			if(state1 == 1){
    				state1 = 2;
    			}
    			break;

    		case 2://R
    			break;

    		case 3://D
    			break;

    		case 4://U
    			if(state1 == 0){
	    			vely1 = 0;
	    			accy1 = -1;
	    			posy1 = 24;
	    			state1 = 1;
    			}
    			break;

    		case 5://Q
    			
    			break;

    		case 6://P
    			
    			break;

    		case 7://B
    			break;

    		case 8://S
    			break;
    	}
    	delay_ms(100);

	}
}