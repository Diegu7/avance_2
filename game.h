#ifndef _GAME_H
#define _GAME_H


#ifndef __ASSEMBLER__
#include "system.h"
#include "screen.h"
#include "keypad.h"


int32_t posx1;
int32_t posy1;
int32_t height1;
int32_t width1;
int32_t vely1;
int32_t accy1;
int32_t velx1;
int32_t accx1;
int32_t state1;

char p1[4][3] = {
	{' ', '0', ' '},
	{'-', '|', '-'},
	{' ', '|', ' '},
	{'/', ' ', '\\'}
};

char p1a[4][3] = {
	{' ', '\\', '0'},
	{'-', ' ', '|'},
	{' ', '/', ' '},
	{'/', '|', ' '}
};



void gameInit();
void gameLoop();
void gamePhysics();
void render();

#endif
#endif