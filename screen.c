#include "screen.h"

void init(){
	set_color(0xff, 0x00);
	clear_screen();
};
void clear_screen(){
	uint16_t data = ((bg | fg) << 8) | ' ';
	for(int i = 0xb800; i < 0xcabf; i = i + 2){
		vgaptr = (uint16_t *)i;
		*vgaptr = data;
	}
	set_cursor(0,0);
};
void set_cursor(uint8_t row, uint8_t column){
	r = row;
	c = column;
	if(r >= 30 || c >= 80){
		r = 0;
		c = 0;
	}
};
void get_cursor(uint8_t *row, uint8_t *column){
	*row = r;
	*column = c;
};
void set_color(uint8_t fgcolor, uint8_t bgcolor){
	fg = fgcolor;
	bg = bgcolor;
	if(fg != 0)
		fg = fg & 0x0f;
	if(bg != 0)
		bg = bg & 0xf0;
};
void get_color(uint8_t *fgcolor, uint8_t *bgcolor){
	*fgcolor = fg;
	*bgcolor = bg;
};
void put_char(uint8_t ch){
	if(ch == '\n'){
		c = 0;
		r++;
		if(r >= 30)
			r = 0;
		return;
	}
	uint16_t pos = ((r * 80) + c) * 2 + 0xb800;
	uint16_t data = ((bg | fg) << 8) | ch;
	vgaptr = (uint16_t *)pos;
	*vgaptr = data;
	colplusplus();
};
void colplusplus(){
	c++;
	if(c >= 80){
		c = 0;
		r++;
		if(r >= 30)
			r = 0;
	}
};
void puts(char *str){
	uint16_t i = 0;
	while(str[i] != 0){
		put_char(str[i]);
		i++;
	}
};
void put_decimal(uint32_t x){
    uint32_t y = x;
    uint16_t c = 0;
    while(y > 9){
        y = divide(y, 10);
        c++;
    }
    y = x;
	char arr[c];
	for(int i = 0; i <= c; i++){
	    arr[c-i] = mod(y, 10) + '0';
	    y = divide(y, 10);
	}
	for(int i = 0; i <= c; i++){
	    put_char(arr[i]);
	}
}
uint32_t divide(uint32_t nu, uint32_t de) {
    uint32_t q = 0;
    uint32_t rem = nu;
    while(rem >= de){
        q++;
        rem = rem - de;
    }
    return q;
}
uint32_t mod(uint32_t nu, uint32_t de) {
    uint32_t q = 0;
    uint32_t rem = nu;
    while(rem >= de){
        q++;
        rem = rem - de;
    }
    return rem;
}